///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file fish.cpp
/// @version 1.0
///
/// Exports data about all fish
///
/// @author Nicholas Tom <tom7@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   03_01_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "fish.hpp"

using namespace std;

namespace animalfarm {

Fish::Fish( float newfavoriteTemp, enum Color newColor, enum Gender newGender ) {
        gender = newGender;         /// Get from the constructor... not all cats are the same gender (this is a has-a relationship)
        species = "Gold";    /// Hardcode this... all cats are the same species (this is a is-a relationship)
	hairColor = newColor;       /// A has-a relationship, so it comes through the constructor
        		       /// An is-a relationship, so it's safe to hardcode.  All cats have the same gestation period.
        favoriteTemp = newfavoriteTemp;             /// A has-a relationship.  Every cat has its own name.
}


const string Fish::speak() {
        return string( "Bubble bubble" );
}


/// Print our Cat and name first... then print whatever information Mammal holds.
void Fish::printInfo() {
        cout << "Fish"  << endl;
        Mammal::printInfo();
	cout << "   Favorite Temperature {" << favoriteTemp  << "]"  << endl;

}

} // namespace animalfarm

