///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file mammal.cpp
/// @version 1.0
///
/// Exports data about all mammals
///
/// @author Nicholas Tom <tom7@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   03_01_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream> 

#include "mammal.hpp"

using namespace std;

namespace animalfarm {
	
void Mammal::printInfo() {
	Animal::printInfo();
	if (scale == 1)
	cout << "   Scale Color = [" << colorName( scaleColor ) << "]" << endl;
	if (hair == 1)	
	cout << "   Hair Color = [" << colorName( hairColor ) << "]" << endl;
	if (feather == 1)
        cout << "   Feather Color = [" << colorName( featherColor ) << "]" << endl;
	if (gestationPeriod > 0)
	cout << "   Gestation Period = [" << gestationPeriod << "]" << endl;
}



} // namespace animalfarm
