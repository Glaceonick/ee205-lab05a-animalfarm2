###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 05a - Animal Farm 2
#
# @file    Makefile
# @version 1.0
#
# @author Nicholas Tom <tom7@hawaii.edu>
# @brief  Lab 05a - Animal Farm 2 - EE 205 - Spr 2021
# @date   03_01_2021
###############################################################################

all: main

main.o:  animal.hpp main.cpp
	g++ -c main.cpp

animal.o: animal.hpp animal.cpp
	g++ -c animal.cpp
	
mammal.o: mammal.hpp mammal.cpp animal.hpp
	g++ -c mammal.cpp

cat.o: cat.cpp cat.hpp mammal.hpp
	g++ -c cat.cpp

dog.o: dog.cpp dog.hpp mammal.hpp
	g++ -c dog.cpp

nunu.o: nunu.cpp nunu.hpp mammal.hpp
	g++ -c nunu.cpp

aku.o: 	aku.cpp aku.hpp mammal.hpp
	g++ -c aku.cpp

palila.o: palila.cpp palila.hpp mammal.hpp
	g++ -c palila.cpp

nene.0: nene.cpp nene.hpp mammal.hpp
	g++ -c nene.cpp

main: main.cpp *.hpp main.o animal.o mammal.o cat.o dog.o fish.o nunu.o aku.o palila.o nene.o
	g++ -o main main.o animal.o mammal.o cat.o dog.o fish.o nunu.o aku.o palila.o nene.o
	
clean:
	rm -f *.o main
