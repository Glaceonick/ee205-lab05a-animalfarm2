///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file bird.hpp
/// @version 1.0
///
/// Exports data about all birds
///
/// @author Nicholas Tom <tom7@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   03_01_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include "mammal.hpp"

using namespace std;


namespace animalfarm {

class Bird : public Mammal {
public:
        string name;

        Bird( float favoriteTemp, enum Color newColor, enum Gender newGender );

        virtual const string speak();

        void printInfo();
};

} // namespace animalfarm

