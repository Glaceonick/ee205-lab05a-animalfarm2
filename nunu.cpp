///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nunu.cpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author Nicholas Tom <tom7@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   03_01_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "nunu.hpp"

using namespace std;

namespace animalfarm {

Nunu::Nunu( bool newisNative, enum Color newColor, enum Gender newGender, float newfavoriteTemp ) {

	isNative = newisNative;
/*	if (isNative == true)
		boo = "true";
	if (isNative == false)
		boo = "false";
*/
	scale = 1;
	gender = newGender;         /// Get from the constructor... not all cats are the same gender (this is a has-a relationship)
        species = "Fistularia chinensis";    /// Hardcode this... all cats are the same species (this is a is-a relationship)
        scaleColor = newColor;       /// A has-a relationship, so it comes through the constructor
        gestationPeriod = 0;         /// An is-a relationship, so it's safe to hardcode.  All cats have the same gestation period.
        favoriteTemp = newfavoriteTemp;             /// A has-a relationship.  Every cat has its own name.
	
}


const string Nunu::speak() {
        return string( "Bubble bubble" );
}


/// Print our Cat and name first... then print whatever information Mammal holds.
void Nunu::printInfo() {
        cout << "Nunu"  << endl;
        cout << "   Is native = [" << isNative << "]" << endl;
	Mammal::printInfo();
        cout << "   Favorite Temperature [" << favoriteTemp  << "]"  << endl;
}

} // namespace animalfarm

